package mx.ucol.httpserver;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClientHandler implements Runnable {
  final Socket socket;

  public ClientHandler(Socket socket) {
    this.socket = socket;
  }

  public void run() {
    DataOutputStream output = null;
    BufferedReader input = null;

    try {
      output = new DataOutputStream(socket.getOutputStream());
      input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

      String received;
      while ((received = input.readLine()) != null) {
        String requestArray[] = received.split(" ");

        if (requestArray[0].equals("GET")) {
          // Get the resource name and read its contents in the /www folder
          // If the resource equals "/" it should open index.html
          System.out.println("Resource: " + requestArray[1]);

          System.out.println(requestArray);

          // No pude hacer la validación, creo que el favicon.ico era el que me lo impedía, y no entra a esa condición
          /*
          // Versión de la tarea
          String fileData = "";
          if (requestArray[1].equals("/")) {
            filename = "/index.html";
          }

          for (String s : readContent(filename)) {
            fileData += s;
          }*/
          //String htmlResponse = fileData;

          //int contentLength = htmlResponse.length();

          // This line should not be modified just yet
          /*output.write("HTTP/1.1 200 OK\r\nContent-Length: " +
            String.valueOf(contentLength) + "\r\n\r\n" + htmlResponse);
          */
          // Update the htmlResponse variable with the file contents

          // Versión del maestro actualizada
          String resourceName = requestArray[1].equals("/") ? "index.html" : requestArray[1];
          String resourcePath = "./www/" + resourceName;
          Path filePath = Paths.get(resourcePath);

          boolean fileExists = Files.exists(filePath, LinkOption.NOFOLLOW_LINKS);

          if(!fileExists) filePath = Paths.get("./www/not-found.html");

          String response = null;
          byte[] fileData = null;
          int contentLength = 0;

          if(fileExists) {
            response = "HTTP/1.1 200 OK\r\n";
          } else {
            response = "HTTP/1.1 404\r\n";
          }

          fileData = Files.readAllBytes(filePath);
          contentLength = fileData.length;
          String mimeType = Files.probeContentType(filePath);
          System.out.println("MIME Type:" + mimeType);

          response += "Content-Type: \r\n" + mimeType + "\r\n";
          response += "Content-Length: " + String.valueOf(contentLength) + "\r\n\r\n";

          output.writeBytes(response);
          output.write(fileData, 0, contentLength);

          // We already sent the response, break the loop
          break;
        }
      }

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        output.close();
        input.close();
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  /* public List<String> readContent(String filename) {
    File myFile = new File("www/" + filename);
    List<String> fileData = new ArrayList<String>();
    try {
      Scanner myReader = new Scanner(myFile);
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        fileData.add(data);
      }
      myReader.close();
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred: " + e.getMessage());
    }
    return fileData;
  } */
}