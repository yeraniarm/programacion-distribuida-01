package mx.ucol.threads;

public class PrimeThread implements Runnable {
    @Override
    public void run() {
        double startProcess = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName()+" counted "+ App.countPrimes(0,App.MAX)+" primes in " + (System.currentTimeMillis()-startProcess)/1000 +" seconds.");
    }
}
