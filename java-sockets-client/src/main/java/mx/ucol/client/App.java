package mx.ucol.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Socket socket;
        DataOutputStream outputStream;
        DataInputStream dataInputStream;
        int port = 3000;

        try {
            socket = new Socket("localhost", port);
            outputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());
            Thread.sleep(3000);
            Scanner input = new Scanner(System.in);
            System.out.println("Introduce your name: ");
            String name = input.next();

            while (true) {
                System.out.println("Introduce message: ");
                String inputMsg = input.next();
                if (inputMsg.equals("exit")) {
                    outputStream.close();
                    socket.close();
                    break;
                }
                outputStream.writeUTF(name + "##" + inputMsg);
                outputStream.flush();

                String serverMessage = dataInputStream.readUTF();
                System.out.println(serverMessage);
            }
        } catch (Exception e) {
            System.err.print(e);
        }
    }
}
