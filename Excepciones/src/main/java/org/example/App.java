package org.example;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class App 
{
    public static void main( String[] args ) {
        File myFile = new File("newFile.txt");
        createFile(myFile);
        writeContent(myFile, "Hola! \nxd");
        readContent(myFile);
        showAbsolutePath(myFile);
    }

    public static void createFile(File myFile) {
        try {
            if (myFile.createNewFile()) {
                System.out.println("File created: " + myFile.getName());
            }
        } catch (IOException e) {
            System.out.println("An error ocurred: " + e.getMessage());
        }
    }

    public static void writeContent(File myFile, String contents) {
        FileWriter writer = null;

        try {
            writer = new FileWriter(myFile);
            writer.write(contents);
        } catch (IOException e) {
            System.out.println("An error ocurred: " + e.getMessage());
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                } else {
                    System.out.println("Writer was not open");
                }
            } catch (IOException e) {
                System.out.println("An error ocurred: " + e.getMessage());
            }
        }
    }

    public static void readContent(File myFile) {
        try {
            Scanner myReader = new Scanner(myFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred: " + e.getMessage());
        }
    }

    public static void showAbsolutePath(File myFile) {
        try {
            System.out.println(myFile.getAbsolutePath());
        } catch (SecurityException e) {
            System.out.println("An error ocurred: " + e.getMessage());
        }
    }

}
