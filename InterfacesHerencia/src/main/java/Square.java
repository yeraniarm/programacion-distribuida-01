public class Square implements Polygon {
    public int sides = 4;
    public float sideLength = 0;

    public Square (float sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public int getSides() {
        return sides;
    }

    @Override
    public float getArea() {
        return sideLength * sideLength;
    }
}
