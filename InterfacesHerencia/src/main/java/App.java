public class App {
    public static void main(String[] args) {
        Polygon rectangle = new Rectangle(2.5f,5);
        System.out.println("Rectángulo\nÁrea: " + rectangle.getArea());
        System.out.println("Lados: " + rectangle.getSides());

        Polygon square = new Square(5);
        System.out.println("\nCuadrado\nÁrea: " + square.getArea());
        System.out.println("Lados: " + square.getSides());

        Polygon circle = new Circle(3);
        System.out.println("\nCírculo\nÁrea: " + circle.getArea());
        System.out.println("Lados: " + circle.getSides());
    }
}
