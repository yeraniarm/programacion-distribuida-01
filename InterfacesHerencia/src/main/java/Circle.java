public class Circle implements Polygon {
    public int sides = 1;
    public float radio = 0;

    public Circle (float radio) {
        this.radio = radio;
    }

    @Override
    public int getSides() {
        return sides;
    }

    @Override
    public float getArea() {
        return (float)Math.pow(radio,2) * (float)Math.PI;
    }
}
