public class Rectangle implements Polygon {
    public int sides = 4;
    public float width = 0;
    public float height = 0;

    public Rectangle (float width, float height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public int getSides() {
        return sides;
    }

    @Override
    public float getArea() {
        return width * height;
    }
}
