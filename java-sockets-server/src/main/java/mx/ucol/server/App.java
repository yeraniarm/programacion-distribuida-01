package mx.ucol.server;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class App {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket;
        Socket socket;
        int port = 3000;
        DataInputStream inputStream;
        DataOutputStream outputStream;
        serverSocket = new ServerSocket(port);

        while (true)
        {
            try
            {
                socket = serverSocket.accept();
                System.out.println("New client connected : " + socket);

                inputStream = new DataInputStream(socket.getInputStream());
                outputStream = new DataOutputStream(socket.getOutputStream());

                System.out.println("Assigning new thread for this client");
                Thread thread = new ClientMsgHandler(socket, inputStream, outputStream);
                thread.start();
            }
            catch (Exception e){
                System.err.print(e);
            }
        }
    }
}
