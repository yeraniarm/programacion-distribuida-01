package mx.ucol.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class ClientMsgHandler extends Thread {
    final Socket socket;
    final DataInputStream dataInputStream;
    final DataOutputStream dataOutputStream;

    public ClientMsgHandler(Socket socket, DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
        this.socket = socket;
        this.dataInputStream = dataInputStream;
        this.dataOutputStream = dataOutputStream;
    }

    @Override
    public void run() {
        String inputData;

        while(true) {
            try {
                inputData = (String) dataInputStream.readUTF();
                String parts[] = inputData.split("##");
                String username = parts[0];
                String usermsg = parts[1];
                System.out.println("[" + username + "]: " + usermsg);
                dataOutputStream.writeUTF("[" + username + "]: " + usermsg);
                dataOutputStream.flush();

                if (inputData.equals("exit")) {
                    socket.close();
                    break;
                }
            } catch(Exception e) {
                System.err.print(e);
            }
        }
    }
}
