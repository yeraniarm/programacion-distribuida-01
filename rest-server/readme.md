# Backend for a ToDo Application

Backend in Java for getting to-dos from a SQLite database using a REST API.
This was done by Yerania Lizbeth Rivera María from 6D.

## Installation
In order to build the project, you will need to assemble the dependencies and then package the project into a jar:

```mvn package```

Then do

```mvn assembly:assembly package```

And finally:

```java -jar target/rest-server-1.0-SNAPSHOT-jar-with-dependencies.jar```

## Usage
The API can respond to the following endpoints:

```curl localhost:3000/api/v1/todos```

Responds with the following JSON:

```
{
   id: 1,
   title: "First ToDo",
   completed: false,
}
```

### Screenshots from clients

![alt text](./resources/images/1.png "Postman")
![alt text](./resources/images/2.png "CMD curl")
