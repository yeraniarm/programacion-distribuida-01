package mx.ucol;

import java.util.ArrayList;

public class Drop {
    private String[] message = new String[10];
    private int currentIndex = 0;
    // private boolean empty = true;
    private String response;

    public synchronized String take() {
        while (currentIndex <= 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("Someone interrupted this thread." + e);
            }
        }

        // empty = true;

        response = this.message[currentIndex];
        currentIndex--;
        System.out.format("Drop[take]: %s%n", currentIndex);
        notifyAll();

        return response;
    }

    public synchronized void put(String message) {
        while (!(currentIndex < 10)) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }

        // empty = false;
        currentIndex++;
        this.message[currentIndex] = message;
        System.out.format("Drop[put]: %s%n", currentIndex);
        notifyAll();
    }
}
