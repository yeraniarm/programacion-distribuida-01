package mx.ucol;

public class App {
    public static void main(String[] args) {
        Drop drop = new Drop();

        for(int i = 0; i < 5; i++) {
            new Thread(new Producer(drop)).start();
            new Thread(new Consumer(drop)).start();
        }

        // Tarea corregida con lo visto en clase
        // Problemas a resolver:

        // - Que los datos del buffer siempre sean consumidos todos hasta el final
        // - Que un consumidor siempre tome al menos 1 dato
        //   **Explicación del problema: En cuanto un productor termina de producir, manda el "DONE" y hace que
        //   todos dejen de producir, así que interrumpe a los demás y no se terminan
        //   de consumir todos los datos
        //   **Posible solución: usar un Vector como buffer(message) en lugar de un Array de Strings
    }
}
